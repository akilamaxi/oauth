package com.atlassian.oauth.client.example;


import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertiesClient {
    public static final String CONSUMER_KEY = "consumer_key";
    public static final String PRIVATE_KEY = "private_key";
    public static final String REQUEST_TOKEN = "request_token";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String SECRET = "secret";
    public static final String JIRA_HOME = "jira_home";


    private final static Map<String, String> DEFAULT_PROPERTY_VALUES = ImmutableMap.<String, String>builder()
            .put(JIRA_HOME, "http://172.20.135.1")
            .put(CONSUMER_KEY, "OauthKey")
            .put(PRIVATE_KEY,
                    "MIICWwIBAAKBgQC+geylYdfOUeSMG4/ufXgAI+ppuyr78FJNY+D5oA99WYNQBU1x\r\n" +
                    "y3w7/nhyg3qyntv9g3Hb7+OV8a7aiW6Ff93u5L1glX3tsLdJbpxWTix+qtE6GVRn\r\n" +
                    "H/s/EWfE1DjLVlO3t9Rdnx/bHfyusk5JxOxxwOu0Q0qEqQgVfF5jilNQ5wIDAQAB\r\n" +
                    "AoGAdR+jwjnZV/t5K7p2TwXMlSl/oOl1j9c8gl/5nP8PBt3VoYjhCCk2hBv7KUw0\r\n" +
                    "T0Tjs4Twp2tg3HruKWV9EY5/fqRsp9ea0tr/HKkwhQNeY2nzqB4lGQJOPOJc8lB1\r\n" +
                    "bv7XGFwzFWhqo/s8X2FJwpJ8vMwjko3c7Bzp8daLxySBeSECQQDj35MrVGILTtVk\r\n" +
                    "15BDXbwYaWi+VIvJUJJZ7DpVWBNWsd69eTuVziG2twm5C286hXMO9fvDf/2x5hy1\r\n" +
                    "EqMOV2S1AkEA1gWmjedMu9Yk8kc2THUA/NjmKJHERBfvyB4P8CbXOzDCHAIfCmlo\r\n" +
                    "ckY2W/04CfZCxmQHH1i1aU6OhHiSgfpcqwJAN5j3GV1PRp8HZcFKi4U1PWeKjNWn\r\n" +
                    "/YhRDOucF/KTVVW0mNoZdaWDiEcEcked7dNdjUrrEHoL9fnFC1Duw7MC+QJARN6i\r\n" +
                    "nu08SwF4+CGkPmw0OikNvyZQ8tyFA29xD4VgIyLog5zHIlGTE5q2aVmNi98b071i\r\n" +
                    "cVrZXZ6yy7wE4SCQawJAAmVW3JBWDffYsV1iR/L7wt7fsI8kb+sYMxthzt5nLZFa\r\n" +
                    "rsC2FDSlq41i9mo5J3KpaGBUDZcEyAwYylXbayguzw=="
                   )
            .build();

    private final String fileUrl;
    private final String propFileName = "config.properties";

    public PropertiesClient() throws Exception {
        fileUrl = "./" + propFileName;
    }

    public Map<String, String> getPropertiesOrDefaults() {
        try {
            Map<String, String> map = toMap(tryGetProperties());
            map.putAll(Maps.difference(map, DEFAULT_PROPERTY_VALUES).entriesOnlyOnRight());
            return map;
        } catch (FileNotFoundException e) {
            tryCreateDefaultFile();
            return new HashMap<>(DEFAULT_PROPERTY_VALUES);
        } catch (IOException e) {
            return new HashMap<>(DEFAULT_PROPERTY_VALUES);
        }
    }

    private Map<String, String> toMap(Properties properties) {
        return properties.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .collect(Collectors.toMap(o -> o.getKey().toString(), t -> t.getValue().toString()));
    }

    private Properties toProperties(Map<String, String> propertiesMap) {
        Properties properties = new Properties();
        propertiesMap.entrySet()
                .stream()
                .forEach(entry -> properties.put(entry.getKey(), entry.getValue()));
        return properties;
    }

    private Properties tryGetProperties() throws IOException {
        InputStream inputStream = new FileInputStream(new File(fileUrl));
        Properties prop = new Properties();
        prop.load(inputStream);
        return prop;
    }

    public void savePropertiesToFile(Map<String, String> properties) {
        OutputStream outputStream = null;
        File file = new File(fileUrl);

        try {
            outputStream = new FileOutputStream(file);
            Properties p = toProperties(properties);
            p.store(outputStream, null);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            closeQuietly(outputStream);
        }
    }

    public void tryCreateDefaultFile() {
        System.out.println("Creating default properties file: " + propFileName);
        tryCreateFile().ifPresent(file -> savePropertiesToFile(DEFAULT_PROPERTY_VALUES));
    }

    private Optional<File> tryCreateFile() {
        try {
            File file = new File(fileUrl);
            file.createNewFile();
            return Optional.of(file);
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    private void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            // ignored
        }
    }
}
